# Freebox
## About
A simple physics simulator.<br/>
Uses: [Glfw3](https://github.com/glfw/glfw "Graphics drawing"), [Chipmunk2D](https://github.com/slembcke/Chipmunk2D "2D physics").
## Controls
Left mouse button to add an object.<br/>
Right-click to delete the object.<br/>
The middle mouse button is used to move objects.<br/>
Space to pause the program.<br/>
Arrows to move the camera.<br/>
The plus and minus key to zoom in and out of the created object.<br/>
Tap four and six on the number bar to reduce and increase the width of the object you are creating.<br/>
Same with two and eight.
## License
[Unlicense](https://unlicense.org/).
## Screenshot
![screenshot1](screenshot1.png)
![screenshot2](screenshot2.png)