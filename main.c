#include <chipmunk/chipmunk.h>
#include <GLFW/glfw3.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define START_WIDTH 25
#define START_HEIGHT 25
#define START_CAM_X 200
#define START_CAM_Y 200
#define GRAVITY cpv(0.0,-100.0)

typedef struct{
int cntvects;
cpVect *vects;
cpVect *rotvects;
cpBody *body;
cpShape *shape;
}figure_t;

figure_t **shapes=NULL;
int cntshapes=0;
cpSpace *space=NULL;

figure_t *createFigure(int cntvects,const cpVect *vects,cpSpace *world,cpVect position,cpFloat mass,cpFloat friction){
figure_t *ret=malloc(sizeof(figure_t));

ret->cntvects=cntvects;

ret->vects=(cpVect*)malloc(sizeof(cpVect)*ret->cntvects);

if(ret->vects==NULL){
fprintf(stderr,"malloc error\n");
free(ret);
return NULL;
}

ret->rotvects=(cpVect*)malloc(sizeof(cpVect)*ret->cntvects);

if(ret->rotvects==NULL){
fprintf(stderr,"malloc error\n");
free(ret->vects);
free(ret);
return NULL;
}

for(int i=0;i!=ret->cntvects;i++){
ret->vects[i]=vects[i];
ret->rotvects[i]=vects[i];
}

float moment=cpMomentForPoly(mass,ret->cntvects,ret->vects,cpvzero,1);

cpBody *body=cpBodyNew(mass,moment);

if(body==NULL){
fprintf(stderr,"cpBodyNew error\n");
free(ret->vects);
free(ret->rotvects);
free(ret);
return NULL;
}

ret->body=cpSpaceAddBody(world,body);

if(ret->body==NULL){
fprintf(stderr,"cpSpaceAddBody error\n");
free(ret->vects);
free(ret->rotvects);
cpBodyFree(body);
free(ret);
return NULL;
}

cpBodySetPosition(ret->body,position);

cpShape *shape=cpPolyShapeNewRaw(ret->body,ret->cntvects,ret->vects,1);

if(shape==NULL){
fprintf(stderr,"cpPolyShapeNewRaw error\n");
free(ret->vects);
free(ret->rotvects);
cpSpaceRemoveBody(world,ret->body);
cpBodyFree(ret->body);
free(ret);
return NULL;
}

ret->shape=cpSpaceAddShape(world,shape);

if(ret->shape==NULL){
fprintf(stderr,"cpSpaceAddShape error\n");
free(ret->vects);
free(ret->rotvects);
cpSpaceRemoveBody(world,ret->body);
cpBodyFree(ret->body);
cpShapeFree(ret->shape);
free(ret);
return NULL;
}

cpShapeSetFriction(ret->shape,friction);

return ret;
}

double fxpos,fypos;
bool buttonrepeat=false;
int forcebody=0;
bool pause=false;

int camx=START_CAM_X;
int camy=START_CAM_Y;

bool up=false;
bool down=false;
bool left=false;
bool right=false;

int sizew=START_WIDTH;
int sizeh=START_HEIGHT;

void msact(GLFWwindow *window,int but,int act,int mods){

if(act==GLFW_PRESS){

if(but==GLFW_MOUSE_BUTTON_LEFT){
double xpos,ypos;
glfwGetCursorPos(window,&xpos,&ypos);

figure_t **figures=(figure_t**)realloc(shapes,sizeof(figure_t*)*(cntshapes+1));

if(figures==NULL){
fprintf(stderr,"realloc error\n");
}else{

shapes=figures;

cpVect vects[4];
vects[0]=cpv(-sizew,-sizeh);
vects[1]=cpv(sizew,-sizeh);
vects[2]=cpv(sizew,sizeh);
vects[3]=cpv(-sizew,sizeh);

ypos=600-ypos;
xpos-=camx;
ypos-=camy;

shapes[cntshapes]=createFigure(4,vects,space,cpv(xpos,ypos),(sizew/5)*(sizeh/5),0.7);

if(shapes[cntshapes]==NULL){

fprintf(stderr,"createFigure error");
shapes=realloc(shapes,sizeof(figure_t*)*(cntshapes));

}else
cntshapes++;
}

}else

if(but==GLFW_MOUSE_BUTTON_RIGHT&&cntshapes!=0&&!buttonrepeat){
double xpos,ypos;
glfwGetCursorPos(window,&xpos,&ypos);
ypos=600-ypos;
xpos-=camx;
ypos-=camy;

cpShape *coll=cpSpacePointQueryNearest(space,cpv(xpos,ypos),0,CP_SHAPE_FILTER_ALL,NULL);

if(coll!=NULL)
for(int i=0;i!=cntshapes;i++)
if(coll==shapes[i]->shape){

free(shapes[i]->vects);
free(shapes[i]->rotvects);

cpSpaceRemoveShape(space,shapes[i]->shape);
cpSpaceRemoveBody(space,shapes[i]->body);

cpShapeFree(shapes[i]->shape);
cpBodyFree(shapes[i]->body);

free(shapes[i]);
for(int j=i+1;j!=cntshapes;j++)shapes[j-1]=shapes[j];

cntshapes--;
shapes=realloc(shapes,sizeof(figure_t*)*cntshapes);

break;
}

}else

if(but==GLFW_MOUSE_BUTTON_MIDDLE&&cntshapes!=0){
double xpos,ypos;
glfwGetCursorPos(window,&xpos,&ypos);
ypos=600-ypos;
xpos-=camx;
ypos-=camy;
cpShape *coll=cpSpacePointQueryNearest(space,cpv(xpos,ypos),0,CP_SHAPE_FILTER_ALL,NULL);

if(coll!=NULL)
for(int i=0;i!=cntshapes;i++)
if(coll==shapes[i]->shape){

forcebody=i;
buttonrepeat=true;
fxpos=xpos;
fypos=ypos;

break;
}

}

}else if(but==GLFW_MOUSE_BUTTON_MIDDLE)buttonrepeat=false;
}

void keyact(GLFWwindow *window,int key,int code,int act,int mode){

if(act==GLFW_PRESS){

if(key==GLFW_KEY_SPACE)pause=!pause;else
if(key==GLFW_KEY_KP_ADD){sizew+=5;sizeh+=5;}else
if(key==GLFW_KEY_KP_SUBTRACT&&sizew!=5&&sizeh!=5){sizew-=5;sizeh-=5;}else
if(key==GLFW_KEY_KP_8)sizeh+=5;else
if(key==GLFW_KEY_KP_2&&sizeh!=5)sizeh-=5;else
if(key==GLFW_KEY_KP_6)sizew+=5;else
if(key==GLFW_KEY_KP_4&&sizew!=5)sizew-=5;

}

if(key==GLFW_KEY_UP)up=!(act==GLFW_RELEASE);else
if(key==GLFW_KEY_DOWN)down=!(act==GLFW_RELEASE);

if(key==GLFW_KEY_LEFT)left=!(act==GLFW_RELEASE);else
if(key==GLFW_KEY_RIGHT)right=!(act==GLFW_RELEASE);

}

void rotatePolygon(const cpVect *vects,cpVect *rotvects,int countvects,cpFloat radian){
for(int i=0;i!=countvects;i++){
rotvects[i].x=vects[i].x*cos(radian)-vects[i].y*sin(radian);
rotvects[i].y=vects[i].x*sin(radian)+vects[i].y*cos(radian);
}
}

int main(void){

GLFWwindow *window;

if(!glfwInit()){
fprintf(stderr,"glfwInit error\n");
exit(EXIT_FAILURE);
}

glfwWindowHint(GLFW_RESIZABLE,GLFW_FALSE);

window=glfwCreateWindow(800,600,"Freebox",NULL,NULL);

if(window==NULL){
glfwTerminate();
fprintf(stderr,"glfwCreateWindow error\n");
exit(EXIT_FAILURE);
}

glfwSetKeyCallback(window,keyact);
glfwSetMouseButtonCallback(window,msact);

glfwMakeContextCurrent(window);

glMatrixMode(GL_PROJECTION);

glLoadIdentity();

glOrtho(0,800,0,600,0,1);
glViewport(0,0,800,600);

glMatrixMode(GL_MODELVIEW);

glDisable(GL_DEPTH_TEST);

glClearColor(0.3,0.3,0.5,0);
glfwSwapInterval(1);

space=cpSpaceNew();
cpSpaceSetGravity(space,GRAVITY);

cpShape *ground=cpSegmentShapeNew(cpSpaceGetStaticBody(space),cpv(20,-50),cpv(400,-50),0);
cpShapeSetFriction(ground,100);
cpSpaceAddShape(space,ground);

cpFloat timeStep=1.0/60.0;

while(!glfwWindowShouldClose(window)){

if(buttonrepeat){
double xpos,ypos;
glfwGetCursorPos(window,&xpos,&ypos);
cpVect hm=cpBodyGetPosition(shapes[forcebody]->body);
cpFloat force=cpBodyGetMass(shapes[forcebody]->body);
cpBodyApplyForceAtWorldPoint(shapes[forcebody]->body,cpv(((xpos-camx)-fxpos)*8*force,(((600-ypos)-camy)-fypos)*8*force),hm);
fxpos=hm.x;
fypos=hm.y;
}

if(up)camy-=10;else
if(down)camy+=10;
if(left)camx+=10;else
if(right)camx-=10;

glClear(GL_COLOR_BUFFER_BIT);

glBegin(GL_LINES);
glVertex2s(20+camx,-50+camy);
glVertex2s(400+camx,-50+camy);
glEnd();

for(int i=0;i!=cntshapes;i++){

cpVect pos=cpBodyGetPosition(shapes[i]->body);
pos.x+=camx;
pos.y+=camy;

cpFloat angle=cpBodyGetAngle(shapes[i]->body);

rotatePolygon(shapes[i]->vects,shapes[i]->rotvects,shapes[i]->cntvects,angle);

glBegin(GL_LINE_LOOP);
for(int j=0;j!=shapes[i]->cntvects-1;j++){
glVertex2f(shapes[i]->rotvects[j].x+pos.x,shapes[i]->rotvects[j].y+pos.y);
glVertex2f(shapes[i]->rotvects[j+1].x+pos.x,shapes[i]->rotvects[j+1].y+pos.y);
}
glEnd();
}

if(!pause)cpSpaceStep(space,timeStep);

glfwSwapBuffers(window);
glfwPollEvents();

}

for(int i=0;i!=cntshapes;i++){
free(shapes[i]->vects);
free(shapes[i]->rotvects);
cpSpaceRemoveShape(space,shapes[i]->shape);
cpSpaceRemoveBody(space,shapes[i]->body);
cpShapeFree(shapes[i]->shape);
cpBodyFree(shapes[i]->body);
free(shapes[i]);
}

free(shapes);

cpShapeFree(ground);
cpSpaceFree(space);

glfwDestroyWindow(window);
glfwTerminate();

return EXIT_SUCCESS;
}
